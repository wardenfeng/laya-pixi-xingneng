main();

async function main() {

    Laya.LayaGL.renderOBJCreate = new Laya.WebGLRenderEngineFactory();
    await Laya.init({ designHeight: 900, designWidth: 1136 });

    const container = new Laya.Sprite();
    Laya.stage.addChild(container);
    const num = 2500;
    const size = 5;
    const num_i = Math.sqrt(num);

    // container.x = num_i * size;
    // container.y = num_i * size;
    // container.pivotX = num_i * size;
    // container.pivotY = num_i * size;


    for (let i = 0; i < num; i++) {
        const s = new Laya.Sprite();
        s.graphics.drawCircle(0, 0, size, 0xff0000);
        
        s.x = Math.floor(i / num_i) * size * 2;
        s.y = (i % num_i) * size * 2;
        container.addChild(s);
    }

        // Move the container to the center
        container.x = Laya.stage.width / 2;
        container.y = Laya.stage.height / 2;
    
        // Center the bunny sprites in local container coordinates
        container.pivotX = num_i * size;
        container.pivotY = num_i * size;

    const update = () => {

        container.rotation++;

        requestAnimationFrame(update);
    }

    update();
}

import Stats from 'stats.js';
var stats = new Stats();
stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild(stats.dom);

const update = () => {

    stats.update();

    requestAnimationFrame(update);
}

update();