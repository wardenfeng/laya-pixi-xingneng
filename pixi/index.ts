import { Application, Assets, Container, Sprite, Graphics } from 'pixi.js';

(async () => {
    // Create a new application
    const app = new Application();

    // Initialize the application
    await app.init({preference:'webgl', background: '#1099bb', resizeTo: window });

    // Append the application canvas to the document body
    document.body.appendChild(app.canvas);

    // Create and add a container to the stage
    const container = new Container();

    app.stage.addChild(container);

    const num = 25000;
    const size = 5;
    const num_i = Math.sqrt(num);

    // Create a 5x5 grid of bunnies in the container
    for (let i = 0; i < num; i++) {
        const graphics = new Graphics();
        graphics.circle(size, size, size);
        graphics.fill(0xc34288, 1);
        const bunny = graphics;

        bunny.x = (i % num_i) * size * 2;
        bunny.y = Math.floor(i / num_i) * size * 2;
        container.addChild(bunny);
    }

    // Move the container to the center
    container.x = app.screen.width / 2;
    container.y = app.screen.height / 2;

    // Center the bunny sprites in local container coordinates
    container.pivot.x = container.width / 2;
    container.pivot.y = container.height / 2;

    // Listen for animate update
    app.ticker.add((time) => {
        // Continuously rotate the container!
        // * use delta to create frame-independent transform *
        container.rotation -= 0.01 * time.deltaTime;
    });
})();


import Stats from 'stats.js';
var stats = new Stats();
stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild(stats.dom);

const update = () => {

    stats.update();

    requestAnimationFrame(update);
}

update();